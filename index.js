console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log(fullName);
	console.log("My full name is" + " " + fullName);

	let age = 40;
	console.log("My current age is: " + " " + age);

	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log(friends);
	console.log("My Friends are:"+ friends)



		let profile = {
			userName: "captain_america",
			fullName: "Steven Rogers",
			age: 40,
			isActive: false,
			
		};
		console.log("My Full Profile:"+ profile)
		console.log(profile);


		let bestfriend = "Bucky Barnes";
		console.log("My bestfriend is:"+ bestfriend);

		const lastLocation = ["Arctic Ocean", "Atlantic Ocean"];		
		console.log("I was found frozen in: " + lastLocation);


		let person = {
		fullName: "Midoriya Izuku",
		age: 15,
		isStudent: true,
		contactNo: ["091234567899", "8123 4567"],
		address: {
		houseNumber: "568",
		city: "Tokyo"
		}
		};
		console.log(person);

// C R E A T E  V A R I A B L E S

		let firstName = "Coleen"
		console.log(firstName)

		let lastName = "Castro"
		console.log(lastName)

		let age1 = 23;
		console.log(age1);

		let hobbies = ["drawing", "video games", "programming"]
		console.log(hobbies)

		let workaddress =  {
			houseNumber: 20223,
			street: "Juan Luna",
			City: "Manila",
			isActive: false,
		};
		console.log(workaddress)
